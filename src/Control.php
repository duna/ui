<?php

namespace Duna\UI;

use App\Components\Flash\Flash;
use Nette\DI\MissingServiceException;
use Nette\InvalidStateException;
use Tracy\Debugger;
use Tracy\ILogger;

/**
 * @property-write string   $presmissionName
 * @property-write callable $callback
 * @package Duna\UI
 */
abstract class Control extends \Nette\Application\UI\Control
{
    /** @var string Name of component used in permissions */
    private $presmissionName = '';

    /** @var callable[] */
    private $callback = [];

    public function __construct(\Nette\Application\UI\Control $parent = null, $name = null)
    {
        parent::__construct();
        if ($parent !== null)
            $parent->addComponent($this, $name);
    }

    /**
     * @param string $presmissionName
     */
    public function setPresmissionName($presmissionName)
    {
        $this->presmissionName = $presmissionName;
    }

    /**
     * @param callable $callback
     */
    public function setCallback(callable $callback)
    {
        $this->callback[] = $callback;
        return $this;
    }

    public function callCallbacks(array $arguments)
    {
        foreach ($this->callback as $callback) {
            call_user_func_array($callback, $arguments);
        }
    }

    public function isAllowedOperation($operation, $name = '')
    {
        try {
            $presenter = $this->getPresenter(true);
            /** @var \Duna\Plugin\SecurityComponent\Authorizator $authorizator */
            $authorizator = $presenter->context->getService('authorizatorComponent');

            /** @var \Nette\Security\IIdentity $identity */
            $identity = $presenter->getUser()->getIdentity();
            if (!$identity)
                throw new InvalidStateException();
            $email = $identity->getData()['email'];
            return $authorizator->isAllowed($email, '_' . $presenter->getName() . '_' . $name, $operation);
        } catch (MissingServiceException $e) {
            return $this->isAllowed($operation);
        } catch (InvalidStateException $e) {
            Debugger::log($e);
            Debugger::log($e->getMessage(), ILogger::WARNING);
            return false;
        }
    }

    public function isAllowed($operation)
    {
        try {
            $presenter = $this->getPresenter(true);
            return $presenter->getUser()->isAllowed($presenter->name, $operation);
        } catch (InvalidStateException $e) {
            Debugger::log($e);
            Debugger::log($e->getMessage(), ILogger::WARNING);
            return false;
        }
    }

    public function flashMessage($message, $type = Flash::INFO)
    {
        try {
            $presenter = $this->getPresenter(true);
            if (isset($presenter['flash']))
                $presenter['flash']->redrawControl();
        } catch (InvalidStateException $e) {
            Debugger::log($e);
            Debugger::log($e->getMessage(), ILogger::WARNING);
        } finally {
            return parent::flashMessage($message, $type);
        }
    }

    abstract public function render(array $params = null);
}