<?php

namespace Duna\Plugin\UI\DI;

use Duna\Console\IEntityConsoleProvider;
use Duna\DI\Extensions\CompilerExtension;
use Duna\Plugin\Manager\Entity\Plugin;
use Duna\Plugin\Manager\IPlugin;
use Kdyby\Doctrine\DI\IEntityProvider;

class Extension extends CompilerExtension implements IPlugin
{

    /**
     * {@inheritDoc}
     */
    public static function getPluginInfo()
    {
        $entity = new Plugin();
        $entity->name = 'UI Controls';
        $entity->description = 'Plugin UI Controls';

        return [
            'plugin' => $entity,
        ];
    }

}