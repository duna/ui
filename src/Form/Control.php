<?php

namespace Duna\UI\Form;

use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Lukáš Černý <LukasCerny@hotmail.com>
 * @property-read array $messages
 */
abstract class Control extends \Duna\UI\Control
{

    /** @var callback[] */
    public $onSuccess = [];
    /** @var callback[] */
    public $onError = [];

    /** @var callable */
    public $onCreate;
    /** @var callable */
    public $onUpdate;
    protected $entity;

    /** @var array [{type => '', message => ''}] */
    private $messages = [];

    public function __construct($parent, $name, EntityManagerInterface $em, $entity)
    {
        //parent::__construct($parent, $name, $em);
        $this->entity = $entity;
        $this->onSuccess[] = [$this, 'formSuccess'];
        $this->onError[] = [$this, 'formError'];
    }

    public function formErrorToMessages(Form $form)
    {
        foreach ($form->getErrors() as $message) {
            $this->messages[] = (object) [
                'type'    => 'error',
                'message' => $message,
            ];
        }
        $this->redrawControl();
    }

    public function formSuccessToMessages(Form $form)
    {
        foreach ($form->getSuccess() as $message) {
            $this->messages[] = (object) [
                'type'    => 'success',
                'message' => $message,
            ];
        }
        $this->redrawControl();
    }

    public function getMessages()
    {
        return $this->messages;
    }

    protected function createForm($name)
    {
        $form = new Form($this, $name);
        $that = $this;

        if ($this->entity === null && $this->onCreate !== null)
            $this->onSuccess[] = function (Form $form) use ($that) {
                call_user_func_array($that->onCreate, [$form, $that->entity, $that]);
            };
        if ($this->entity !== null && $this->onUpdate !== null)
            $this->onSuccess[] = function (Form $form) use ($that) {
                call_user_func_array($that->onUpdate, [$form, $that->entity, $that]);
            };

        foreach ($this->onSuccess as $callback) {
            $form->onSuccess[] = $callback;
        }
        $form->onSuccess[] = [$this, 'formSuccessToMessages'];

        foreach ($this->onError as $callback) {
            $form->onError[] = $callback;
        }
        $form->onError[] = [$this, 'formErrorToMessages'];

        return $form;
    }

    public function formSuccess(Form $form, $values){}

    public function formError(Form $form){}

    public function setDefaults(Form $form){}
}
