<?php

namespace Duna\UI\Form\Generator;


use Nette\ComponentModel\IContainer;

interface IGenerator
{
    /**
     * @return \Duna\UI\Form\Form
     */
    public function generateForm(IContainer $container = null, $name = null);
}