<?php

namespace Duna\UI\Form\Generator;


use Duna\UI\Form\Form;
use Duna\UI\Form\Inputs\Types;
use Nette\ComponentModel\IContainer;
use Nette\InvalidArgumentException;

class Json implements IGenerator
{
    /** @var \Duna\UI\Form\Form */
    private $form;
    private $formStrucutre;
    public function __construct(array $formStrucutre)
    {
        $this->formStrucutre = $formStrucutre;
    }

    public function generateForm(IContainer $container = null, $name = null)
    {
        $form = new Form($container, $name);
        foreach ($this->formStrucutre as $input) {
            $this->generateInput($form, (array) $input);
        }
        $form->addSubmit('send', 'Potvrdit');
        $this->form = $form;
        return $form;
    }

    /**
     * @param \Duna\UI\Form\Form $form
     * @param array              $inputData
     * @return \Nette\Forms\Controls\BaseControl
     */
    public function generateInput(Form $form, array $inputData)
    {
        $input = null;
        $label = array_key_exists('label', $inputData)?$inputData['label']:null;
        switch ($inputData['type']) {
            case Types::INTEGER:
                $input = $form->addInteger($inputData['name'], $label);
                break;
            case Types::STRING:
                $input = $form->addText($inputData['name'], $label);
                break;
            case Types::SELECT:
                $input = $form->addSelect($inputData['name'], $label, (array) $inputData['options']);
                break;
            default:
                throw new InvalidArgumentException("Bad type of input (" . $inputData['type'] . ").");
        }
        $default = array_key_exists('default', $inputData)?$inputData['default']:null;
        if ($default !== null) {
            if ($inputData['type'] == Types::SELECT)
                $input->setDefaultValue(array_search($inputData['default'], $inputData['options']));
            else
                $input->setDefaultValue($inputData['default']);
        }
        if (array_key_exists('required', $inputData))
            $input->setRequired($inputData['required']);

        return $input;
    }

}