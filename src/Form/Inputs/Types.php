<?php

namespace Duna\UI\Form\Inputs;
class Types
{
    const INTEGER = 'integer';
    const STRING = 'string';
    const SELECT = 'select';
}