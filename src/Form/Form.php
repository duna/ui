<?php

namespace Duna\UI\Form;


class Form extends \Nette\Application\UI\Form
{
    protected $configuration = [
        'labels'   => [],
        'required' => [],
    ];
    private $success = [];

    public function addSuccess($message)
    {
        $this->success[] = $message;
    }

    public function getSuccess()
    {
        return $this->success;
    }

    public function addLabel($name, $value)
    {
        $this->configuration['label'][$name] = $value;
        return $this;
    }

    public function addRequired($name, $value)
    {
        $this->configuration['required'][$name] = $value;
        return $this;
    }

    public function generateControlEntity(\ReflectionClass $reflection)
    {
        foreach ($reflection->getProperties() as $property) {
            if ($property->isPublic() || $property->isProtected()) {
                $doc = $property->getDocComment();
                $type = 'string';
                preg_match_all("/^[\s\*\/]+(@.+)/m", $doc, $matches);
                foreach ($matches as $match) {
                    if (substr($match[1], 0, 4) != '@ORM')
                        continue;
                    if (!preg_match("/type=[\"\'](\w+)[\"\']/", $match[1], $temp))
                        continue;
                    $type = $temp[1];
                    break;
                }
                $control = $this->createControl($property->name, $type);
            }
        }
    }

    private function createControl($name, $type, $required = false)
    {
        $name = self::toSnakeCase($name);
        $label = isset($this->configuration['label'][$name]) ? $this->configuration['label'][$name] : null;
        $control = null;
        switch ($type) {
            case "integer":
            case "int":
                $control = $this->addInteger($name, $label);
                break;
            case "string":
                $control = $this->addText($name, $label);
                break;
        }
        return $control;
    }

    public static function toSnakeCase($input)
    {
        $output = '';
        foreach (str_split($input) as $char) {
            if (strtolower($char) != $char && strlen($output)) {
                $output .= '_';
            }
            $output .= strtolower($char);
        }
        return $output;
    }
}