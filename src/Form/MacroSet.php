<?php

namespace Duna\UI\Form;

use Duna\UI\Form\RenderB3;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\PhpWriter;
use Tracy\Debugger;

class MacroSet extends \Latte\Macros\MacroSet
{
    public static function install(Compiler $compiler)
    {
        $me = new static($compiler);

        $me->addMacro('formGenerator', [$me, 'macroFormGenerator']);
    }

    public function macroFormGenerator(MacroNode $node, PhpWriter $writer)
    {
        $name = $node->tokenizer->fetchWord();
        if ($name === false) {
            throw new CompileException('Missing form name in ' . $node->getNotation());
        }
        $node->replaced = true;
        $node->tokenizer->reset();
        return $writer->write(
            "/* line $node->startLine */\n"
            . 'echo Duna\UI\Form\MacroSet::renderFormBegin($form = $_form = $this->global->formsStack[] = '
            . ($name[0] === '$' ? 'is_object(%node.word) ? %node.word : ' : '')
            . '$this->global->uiControl[%node.word], %node.array);'
        );
    }

    /**
     * Renders form begin.
     * @return string
     */
    public static function renderFormBegin(Form $form, array $attrs, $withTags = true)
    {
        Debugger::barDump($form);
        $form->setRenderer(new \Duna\UI\Form\Rendering\RenderB3());
        $form->fireRenderEvents();
        foreach ($form->getControls() as $control) {
            $control->setOption('rendered', false);
        }
        $el = $form->getElementPrototype();
        $el->action = (string) $el->action;
        $el = clone $el;
        if ($form->isMethod('get')) {
            $el->action = preg_replace('~\?[^#]*~', '', $el->action, 1);
        }
        $el->addAttributes($attrs);
        return $withTags ? $el->startTag() : $el->attributes();
    }
}