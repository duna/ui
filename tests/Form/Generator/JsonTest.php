<?php

namespace Duna\UI\Form\Generator;

use Duna\UI\Form\Form;
use Duna\UI\Form\Inputs\Types;
use Nette\Forms\Controls\BaseControl;
use PHPUnit\Framework\TestCase;

class JsonTest extends TestCase
{
    public function testSimple()
    {
        $expected = new Form();
        $expected->addSubmit('send');
        $this->compareForms($expected, (new Json([]))->generateForm());
    }

    public function testSimple01()
    {
        $expected = new Form();
        $expected->addText('text');
        $expected->addSubmit('send');
        $this->compareForms($expected, (new Json([['type' => Types::STRING, 'name' => 'text']]))->generateForm());
    }

    public function compareForms(Form $expected, Form $form)
    {
        foreach ($expected->getControls() as $index => $control) {
            $temp = $form[$control->name];
            $this->assertSame($control->name, $temp->name);
            $this->assertSame($control->name, $temp->name);
        }

    }

    public function compareControls(BaseControl $expected, BaseControl $control) {
        //switch
    }
}