<?php
/**
 * Created by PhpStorm.
 * User: leo
 * Date: 10/20/17
 * Time: 2:04 PM
 */

namespace Duna\UI\Tests;

use Duna\UI\Control;
use PHPUnit\Framework\TestCase;

class ControlTest extends TestCase
{

    public function testCallback()
    {
        $mock = $this->getMockBuilder(Control::class)
            ->disableOriginalConstructor()
            ->getMock();
        /** @var Control $control */
        $control = new $mock;

        $mockCall = $this->getMockBuilder(\ControlCallback::class)
            ->getMock();

        $mockCall->expects($this->once())
            ->method('callMe01')
            ->with([]);

        $mock->callback = [new $mockCall, 'callMe01'];
        $mock->callCallbacks([]);
    }
}
